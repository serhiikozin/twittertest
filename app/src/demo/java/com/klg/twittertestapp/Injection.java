package com.klg.twittertestapp;

import android.content.Context;
import android.support.annotation.NonNull;

import com.klg.twittertestapp.data.TwitterRepository;
import com.klg.twittertestapp.data.local.LocalDataSource;
import com.klg.twittertestapp.data.local.SharedPreferenceTwitter;
import com.klg.twittertestapp.data.remote.RemoteDataSource;

public class Injection {
    public static TwitterRepository provideTasksRepository(@NonNull Context context) {
        return TwitterRepository.getInstance(RemoteDataSource.getInstance(),
                LocalDataSource.getInstance(), SharedPreferenceTwitter.getInstance(context));
    }
}
