package com.klg.twittertestapp.splash;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.klg.twittertestapp.data.TwitterRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SplashViewModel extends AndroidViewModel {
    private TwitterRepository mTwitterRepository;

    public MutableLiveData<Boolean> authLiveData = new MutableLiveData<>();

    public SplashViewModel(@NonNull Application application, TwitterRepository twitterRepository) {
        super(application);
        mTwitterRepository = twitterRepository;
    }

    @SuppressLint("CheckResult")
    public void authUser() {
        mTwitterRepository
                .twitterAuth()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(auth -> authLiveData.postValue(true),
                        error -> authLiveData.postValue(true));
    }
}
