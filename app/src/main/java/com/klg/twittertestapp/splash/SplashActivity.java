package com.klg.twittertestapp.splash;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.klg.twittertestapp.R;
import com.klg.twittertestapp.ViewModelFactory;
import com.klg.twittertestapp.home.HomeActivity;
import com.klg.twittertestapp.util.NetworkUtils;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        SplashViewModel splashViewModel = obtainViewModel(this);
        initLiveData(splashViewModel);
        if (NetworkUtils.isOnline(this)) {
            splashViewModel.authUser();
        } else {
            showError(getResources().getString(R.string.check_internet));
        }
    }

    private void initLiveData(SplashViewModel splashViewModel) {
        splashViewModel.authLiveData.observe(this, flagAuth -> {
            if (flagAuth != null) {
                if (flagAuth) {
                    openHomeScreen();
                } else {
                    showError(getResources().getString(R.string.auth_error));
                }
            }
        });
    }

    private SplashViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(SplashViewModel.class);
    }

    private void showError(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void openHomeScreen() {
        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        finish();
    }
}
