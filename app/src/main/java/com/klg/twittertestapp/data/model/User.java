package com.klg.twittertestapp.data.model;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    private Integer id;
    @SerializedName("id_str")
    private String idStr;
    @SerializedName("name")
    private String name;
    @SerializedName("screen_name")
    private String screenName;
    @SerializedName("location")
    private String location;
    @SerializedName("description")
    private String description;
    @SerializedName("url")
    private String url;
    @SerializedName("entities")
    private Entities_ entities;
    @SerializedName("protected")
    private Boolean _protected;
    @SerializedName("followers_count")
    private Integer followersCount;
    @SerializedName("friends_count")
    private Integer friendsCount;
    @SerializedName("listed_count")
    private Integer listedCount;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("favourites_count")
    private Integer favouritesCount;
    @SerializedName("utc_offset")
    private Object utcOffset;
    @SerializedName("time_zone")
    private Object timeZone;
    @SerializedName("geo_enabled")
    private Boolean geoEnabled;
    @SerializedName("verified")
    private Boolean verified;
    @SerializedName("statuses_count")
    private Integer statusesCount;
    @SerializedName("lang")
    private String lang;
    @SerializedName("contributors_enabled")
    private Boolean contributorsEnabled;
    @SerializedName("is_translator")
    private Boolean isTranslator;
    @SerializedName("is_translation_enabled")
    private Boolean isTranslationEnabled;
    @SerializedName("profile_background_color")
    private String profileBackgroundColor;
    @SerializedName("profile_background_image_url")
    private String profileBackgroundImageUrl;
    @SerializedName("profile_background_image_url_https")
    private String profileBackgroundImageUrlHttps;
    @SerializedName("profile_background_tile")
    private Boolean profileBackgroundTile;
    @SerializedName("profile_image_url")
    private String profileImageUrl;
    @SerializedName("profile_image_url_https")
    private String profileImageUrlHttps;
    @SerializedName("profile_banner_url")
    private String profileBannerUrl;
    @SerializedName("profile_link_color")
    private String profileLinkColor;
    @SerializedName("profile_sidebar_border_color")
    private String profileSidebarBorderColor;
    @SerializedName("profile_sidebar_fill_color")
    private String profileSidebarFillColor;
    @SerializedName("profile_text_color")
    private String profileTextColor;
    @SerializedName("profile_use_background_image")
    private Boolean profileUseBackgroundImage;
    @SerializedName("has_extended_profile")
    private Boolean hasExtendedProfile;
    @SerializedName("default_profile")
    private Boolean defaultProfile;
    @SerializedName("default_profile_image")
    private Boolean defaultProfileImage;
    @SerializedName("following")
    private Object following;
    @SerializedName("follow_request_sent")
    private Object followRequestSent;
    @SerializedName("notifications")
    private Object notifications;
    @SerializedName("translator_type")
    private String translatorType;

    public Integer getId() {
        return id;
    }

    public String getIdStr() {
        return idStr;
    }

    public String getName() {
        return name;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public Entities_ getEntities() {
        return entities;
    }

    public Boolean getProtected() {
        return _protected;
    }

    public Integer getFollowersCount() {
        return followersCount;
    }

    public Integer getFriendsCount() {
        return friendsCount;
    }

    public Integer getListedCount() {
        return listedCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public Integer getFavouritesCount() {
        return favouritesCount;
    }

    public Object getUtcOffset() {
        return utcOffset;
    }

    public Object getTimeZone() {
        return timeZone;
    }

    public Boolean getGeoEnabled() {
        return geoEnabled;
    }

    public Boolean getVerified() {
        return verified;
    }

    public Integer getStatusesCount() {
        return statusesCount;
    }

    public String getLang() {
        return lang;
    }

    public Boolean getContributorsEnabled() {
        return contributorsEnabled;
    }

    public Boolean getIsTranslator() {
        return isTranslator;
    }

    public Boolean getIsTranslationEnabled() {
        return isTranslationEnabled;
    }

    public String getProfileBackgroundColor() {
        return profileBackgroundColor;
    }

    public String getProfileBackgroundImageUrl() {
        return profileBackgroundImageUrl;
    }

    public String getProfileBackgroundImageUrlHttps() {
        return profileBackgroundImageUrlHttps;
    }

    public Boolean getProfileBackgroundTile() {
        return profileBackgroundTile;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public String getProfileImageUrlHttps() {
        return profileImageUrlHttps;
    }

    public String getProfileBannerUrl() {
        return profileBannerUrl;
    }

    public String getProfileLinkColor() {
        return profileLinkColor;
    }

    public String getProfileSidebarBorderColor() {
        return profileSidebarBorderColor;
    }

    public String getProfileSidebarFillColor() {
        return profileSidebarFillColor;
    }

    public String getProfileTextColor() {
        return profileTextColor;
    }

    public Boolean getProfileUseBackgroundImage() {
        return profileUseBackgroundImage;
    }

    public Boolean getHasExtendedProfile() {
        return hasExtendedProfile;
    }

    public Boolean getDefaultProfile() {
        return defaultProfile;
    }

    public Boolean getDefaultProfileImage() {
        return defaultProfileImage;
    }

    public Object getFollowing() {
        return following;
    }

    public Object getFollowRequestSent() {
        return followRequestSent;
    }

    public Object getNotifications() {
        return notifications;
    }

    public String getTranslatorType() {
        return translatorType;
    }
}