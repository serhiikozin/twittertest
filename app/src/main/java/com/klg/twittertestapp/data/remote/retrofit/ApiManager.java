package com.klg.twittertestapp.data.remote.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.klg.twittertestapp.data.constant.ConstantTwitterTest;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {

    private static ApiManager sInstance;
    private static String mAuthorizationHeader = "";
    private TwitterService mApiService;

    private ApiManager(String auth) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = getOkHttp(auth);
        Retrofit.Builder builder = getRetrofitBuilder();
        Retrofit retrofit = builder.client(httpClient).build();
        mApiService = retrofit.create(TwitterService.class);
    }

    public static ApiManager getInstance(String auth) {
        if (sInstance == null || !auth.equals(mAuthorizationHeader)) {
            mAuthorizationHeader = auth;
            sInstance = new ApiManager(auth);
        }
        return sInstance;
    }

    public TwitterService getService() {
        return mApiService;
    }

    private OkHttpClient getOkHttp(String auth) {
        return new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Authorization", auth)
                            .addHeader("Accept", "application/json")
                            .method(original.method(), original.body());
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }).build();
    }

    private Retrofit.Builder getRetrofitBuilder() {
        Gson gson = new GsonBuilder().create();
        return new Retrofit.Builder()
                .baseUrl(ConstantTwitterTest.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
    }
}