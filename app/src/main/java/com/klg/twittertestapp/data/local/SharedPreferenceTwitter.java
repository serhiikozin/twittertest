package com.klg.twittertestapp.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.klg.twittertestapp.data.SharedPreferenceDataSource;

public class SharedPreferenceTwitter implements SharedPreferenceDataSource {

    @Nullable
    private static SharedPreferenceTwitter INSTANCE;

    private SharedPreferences sharedPreferences;

    private static final String SP_NAME = "twitter";
    private static final String IS_LOGIN = "is_login";
    private static final String TOKEN = "token";


    private SharedPreferenceTwitter(Context context) {
        sharedPreferences = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferenceTwitter getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SharedPreferenceTwitter(context);
        }
        return INSTANCE;
    }

    @Override
    public boolean isAuthorised() {
        return retrieveBoolean(IS_LOGIN, false);
    }

    @Override
    public void setAuthorization(boolean flag) {
        saveBoolean(IS_LOGIN, flag);
    }

    @Override
    public void setToken(String token) {
        saveString(TOKEN, token);
    }

    @Override
    public String getToken() {
        return retrieveString(TOKEN, "");
    }

    private void saveBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    private boolean retrieveBoolean(String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    private void saveString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private String retrieveString(String key, String defValue) {
        return sharedPreferences.getString(key, defValue);
    }
}

