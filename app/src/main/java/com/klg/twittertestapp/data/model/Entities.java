package com.klg.twittertestapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Entities {

@SerializedName("hashtags")
private List<Object> hashtags = null;
@SerializedName("symbols")
private List<Object> symbols = null;
@SerializedName("user_mentions")
private List<Object> userMentions = null;
@SerializedName("urls")
private List<Url> urls = null;

public List<Object> getHashtags() {
return hashtags;
}

public List<Object> getSymbols() {
return symbols;
}

public List<Object> getUserMentions() {
return userMentions;
}

public List<Url> getUrls() {
return urls;
}
}