package com.klg.twittertestapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Description {

    @SerializedName("urls")
    private List<Object> urls = null;

    public List<Object> getUrls() {
        return urls;
    }
}