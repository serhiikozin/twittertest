package com.klg.twittertestapp.data.model;

import com.google.gson.annotations.SerializedName;

public class AuthenticatedTwitter {
    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("access_token")
    private String accessToken;

    public String getTokenType() {
        return tokenType;
    }

    public String getAccessToken() {
        return accessToken;
    }

    @Override
    public String toString() {
        return tokenType + " " + accessToken;
    }
}