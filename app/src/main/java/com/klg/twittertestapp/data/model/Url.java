package com.klg.twittertestapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Url {
    @SerializedName("url")
    private String url;
    @SerializedName("expanded_url")
    private String expandedUrl;
    @SerializedName("display_url")
    private String displayUrl;
    @SerializedName("indices")
    private List<Integer> indices = null;

    public String getUrl() {
        return url;
    }

    public String getExpandedUrl() {
        return expandedUrl;
    }

    public String getDisplayUrl() {
        return displayUrl;
    }

    public List<Integer> getIndices() {
        return indices;
    }
}