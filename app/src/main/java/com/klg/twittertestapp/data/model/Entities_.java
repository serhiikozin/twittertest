package com.klg.twittertestapp.data.model;

import com.google.gson.annotations.SerializedName;

public class Entities_ {

@SerializedName("url")
private Url_ url;
@SerializedName("description")
private Description description;

public Url_ getUrl() {
return url;
}

public void setUrl(Url_ url) {
this.url = url;
}

public Description getDescription() {
return description;
}

public void setDescription(Description description) {
this.description = description;
}

}