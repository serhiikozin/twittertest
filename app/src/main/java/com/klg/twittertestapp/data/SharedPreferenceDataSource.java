package com.klg.twittertestapp.data;

public interface SharedPreferenceDataSource {

    boolean isAuthorised();

    void setAuthorization(boolean flag);

    void setToken(String token);

    String getToken();
}
