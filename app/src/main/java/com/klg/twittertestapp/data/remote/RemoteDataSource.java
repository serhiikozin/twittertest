package com.klg.twittertestapp.data.remote;

import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;

import com.klg.twittertestapp.data.DataSourceTwitter;
import com.klg.twittertestapp.data.constant.ConstantTwitterTest;
import com.klg.twittertestapp.data.model.AuthenticatedTwitter;
import com.klg.twittertestapp.data.model.UserTimeline;
import com.klg.twittertestapp.data.remote.retrofit.ApiManager;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

public class RemoteDataSource implements DataSourceTwitter {

    @Nullable
    private static RemoteDataSource INSTANCE;
    private CompositeDisposable disposable;
    private String mAccessToken = "";
    private String mUserName = "";
    private int mMaxId = 0;

    private RemoteDataSource() {
        disposable = new CompositeDisposable();
    }

    public static RemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RemoteDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void clear() {
        if (disposable != null) {
            disposable.clear();
        }
    }

    @Override
    public Single<AuthenticatedTwitter> twitterAuth() {
        String base64Encoded = getBase64Encoded(
                concatenateKeyAndSecret(ConstantTwitterTest.CONSUMER_KEY, ConstantTwitterTest.CONSUMER_SECRET));
        return ApiManager
                .getInstance("Basic " + base64Encoded)
                .getService()
                .loginTwitter(ConstantTwitterTest.GRANT_TYPE)
                .doOnSuccess(authenticatedTwitter -> {
                    if (authenticatedTwitter.getAccessToken() != null) {
                        mAccessToken = authenticatedTwitter.getTokenType() + " " + authenticatedTwitter.getAccessToken();
                    }
                });
    }

    @Override
    public Single<List<UserTimeline>> getUserTimelines(String userName, int count, String token) {
        mUserName = userName;
        return ApiManager
                .getInstance(token)
                .getService()
                .downloadUserTimeline(userName, count);
    }

    @Override
    public Single<List<UserTimeline>> getOldUserTimelines(String userName, int count, String token) {
        changedMaxId(userName, count);
        mUserName = userName;
        return ApiManager
                .getInstance(token)
                .getService()
                .downloadOldUserTimeline(userName, mMaxId, count);
    }

    @Override
    public Single<List<UserTimeline>> getUserTimelinesWithoutAuth(String userName, int count) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Single<List<UserTimeline>> getOldUserTimelinesWithoutAuth(String userName, int count) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isUserAuthorised() {
        throw new UnsupportedOperationException();
    }

    private boolean changedMaxId(String userName, int count) {
        if (mUserName.equals(userName)) {
            mMaxId += count - 1;
            return true;
        } else {
            mMaxId = 0;
            return false;
        }
    }

    private String concatenateKeyAndSecret(String consumerKey, String consumerSecret) {
        String urlApiKey = "";
        String urlApiSecret = "";
        try {
            urlApiKey = URLEncoder.encode(consumerKey, "UTF-8");
            urlApiSecret = URLEncoder.encode(consumerSecret, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return urlApiKey + ":" + urlApiSecret;
    }

    private String getBase64Encoded(String keyEndSecret) {
        return Base64.encodeToString(keyEndSecret.getBytes(), Base64.NO_WRAP);
    }
}
