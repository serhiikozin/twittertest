package com.klg.twittertestapp.data.model;

import com.google.gson.annotations.SerializedName;

public class UserTimeline {

@SerializedName("created_at")
private String createdAt;
@SerializedName("id")
private Long id;
@SerializedName("id_str")
private String idStr;
@SerializedName("text")
private String text;
@SerializedName("truncated")
private Boolean truncated;
@SerializedName("entities")
private Entities entities;
@SerializedName("source")
private String source;
@SerializedName("in_reply_to_status_id")
private Object inReplyToStatusId;
@SerializedName("in_reply_to_status_id_str")
private Object inReplyToStatusIdStr;
@SerializedName("in_reply_to_user_id")
private Object inReplyToUserId;
@SerializedName("in_reply_to_user_id_str")
private Object inReplyToUserIdStr;
@SerializedName("in_reply_to_screen_name")
private Object inReplyToScreenName;
@SerializedName("user")
private User user;
@SerializedName("geo")
private Object geo;
@SerializedName("coordinates")
private Object coordinates;
@SerializedName("place")
private Object place;
@SerializedName("contributors")
private Object contributors;
@SerializedName("is_quote_status")
private Boolean isQuoteStatus;
@SerializedName("retweet_count")
private Integer retweetCount;
@SerializedName("favorite_count")
private Integer favoriteCount;
@SerializedName("favorited")
private Boolean favorited;
@SerializedName("retweeted")
private Boolean retweeted;
@SerializedName("possibly_sensitive")
private Boolean possiblySensitive;
@SerializedName("lang")
private String lang;

public String getCreatedAt() {
return createdAt;
}

public Long getId() {
return id;
}

public String getIdStr() {
return idStr;
}

public String getText() {
return text;
}

public Boolean getTruncated() {
return truncated;
}

public Entities getEntities() {
return entities;
}

public String getSource() {
return source;
}

public Object getInReplyToStatusId() {
return inReplyToStatusId;
}

public Object getInReplyToStatusIdStr() {
return inReplyToStatusIdStr;
}

public Object getInReplyToUserId() {
return inReplyToUserId;
}

public Object getInReplyToUserIdStr() {
return inReplyToUserIdStr;
}

public Object getInReplyToScreenName() {
return inReplyToScreenName;
}

public User getUser() {
return user;
}

public Object getGeo() {
return geo;
}

public Object getCoordinates() {
return coordinates;
}

public Object getPlace() {
return place;
}

public Object getContributors() {
return contributors;
}

public Boolean getIsQuoteStatus() {
return isQuoteStatus;
}

public Integer getRetweetCount() {
return retweetCount;
}

public Integer getFavoriteCount() {
return favoriteCount;
}

public Boolean getFavorited() {
return favorited;
}

public Boolean getRetweeted() {
return retweeted;
}

public Boolean getPossiblySensitive() {
return possiblySensitive;
}

public String getLang() {
return lang;
}
}