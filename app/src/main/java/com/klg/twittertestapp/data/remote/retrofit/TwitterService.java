package com.klg.twittertestapp.data.remote.retrofit;

import com.klg.twittertestapp.data.model.AuthenticatedTwitter;
import com.klg.twittertestapp.data.model.UserTimeline;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TwitterService {

    @FormUrlEncoded
    @POST("oauth2/token")
    Single<AuthenticatedTwitter> loginTwitter(
            @Field("grant_type") String grantType);

    @GET("1.1/statuses/user_timeline.json")
    Single<List<UserTimeline>> downloadOldUserTimeline(
            @Query("screen_name") String screenName,
            @Query("max_id") int maxId,
            @Query("count") int count);

    @GET("1.1/statuses/user_timeline.json")
    Single<List<UserTimeline>> downloadUserTimeline(
            @Query("screen_name") String screenName,
            @Query("count") int count);
}