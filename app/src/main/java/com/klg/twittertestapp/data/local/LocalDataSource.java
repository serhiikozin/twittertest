package com.klg.twittertestapp.data.local;

import android.support.annotation.Nullable;

import com.klg.twittertestapp.data.DataSourceTwitter;
import com.klg.twittertestapp.data.model.AuthenticatedTwitter;
import com.klg.twittertestapp.data.model.UserTimeline;

import java.util.List;

import io.reactivex.Single;

public class LocalDataSource implements DataSourceTwitter {

    @Nullable
    private static LocalDataSource INSTANCE;

    private LocalDataSource() {
    }

    public static LocalDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LocalDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void clear() {

    }

    @Override
    public Single<AuthenticatedTwitter> twitterAuth() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Single<List<UserTimeline>> getUserTimelines(String userName, int count, String auth) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Single<List<UserTimeline>> getOldUserTimelines(String userName, int count, String auth) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Single<List<UserTimeline>> getUserTimelinesWithoutAuth(String userName, int count) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Single<List<UserTimeline>> getOldUserTimelinesWithoutAuth(String userName, int count) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isUserAuthorised() {
        throw new UnsupportedOperationException();
    }
}
