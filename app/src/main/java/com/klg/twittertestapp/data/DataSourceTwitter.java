package com.klg.twittertestapp.data;

import com.klg.twittertestapp.data.model.AuthenticatedTwitter;
import com.klg.twittertestapp.data.model.UserTimeline;

import java.util.List;

import io.reactivex.Single;

public interface DataSourceTwitter {

    void clear();

    Single<AuthenticatedTwitter> twitterAuth();

    Single<List<UserTimeline>> getUserTimelines(String userName, int count, String auth);

    Single<List<UserTimeline>> getOldUserTimelines(String userName, int count, String auth);

    Single<List<UserTimeline>> getUserTimelinesWithoutAuth(String userName, int count);

    Single<List<UserTimeline>> getOldUserTimelinesWithoutAuth(String userName, int count);

    boolean isUserAuthorised();
}
