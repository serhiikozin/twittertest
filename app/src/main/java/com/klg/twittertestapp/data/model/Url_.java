package com.klg.twittertestapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Url_ {

    @SerializedName("urls")
    private List<Url__> urls = null;

    public List<Url__> getUrls() {
        return urls;
    }

    public void setUrls(List<Url__> urls) {
        this.urls = urls;
    }

}