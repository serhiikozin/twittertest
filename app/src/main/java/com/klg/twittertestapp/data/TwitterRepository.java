package com.klg.twittertestapp.data;

import android.support.annotation.Nullable;

import com.klg.twittertestapp.data.local.LocalDataSource;
import com.klg.twittertestapp.data.model.AuthenticatedTwitter;
import com.klg.twittertestapp.data.model.UserTimeline;
import com.klg.twittertestapp.data.remote.RemoteDataSource;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

public class TwitterRepository implements DataSourceTwitter {

    @Nullable
    private static TwitterRepository INSTANCE;

    private DataSourceTwitter mRemoteDataSource;
    private DataSourceTwitter mLocalDataSource;
    private SharedPreferenceDataSource mSharedPreference;

    private TwitterRepository(RemoteDataSource remoteDataSource,
                              LocalDataSource localDataSource,
                              SharedPreferenceDataSource sharedPreference) {
        mRemoteDataSource = remoteDataSource;
        mLocalDataSource = localDataSource;
        mSharedPreference = sharedPreference;
    }

    public static TwitterRepository getInstance(RemoteDataSource remoteDataSource,
                                                LocalDataSource localDataSource,
                                                SharedPreferenceDataSource sharedPreferenceDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new TwitterRepository(remoteDataSource, localDataSource, sharedPreferenceDataSource);
        }
        return INSTANCE;
    }

    @Override
    public void clear() {
        mRemoteDataSource.clear();
        mLocalDataSource.clear();
    }

    @Override
    public Single<AuthenticatedTwitter> twitterAuth() {
        return mRemoteDataSource.twitterAuth().doOnSuccess(auth -> {
                    if (auth.getAccessToken() != null &&
                            !auth.getAccessToken().isEmpty()) {
                        mSharedPreference.setToken(auth.getTokenType() + " " + auth.getAccessToken());
                        mSharedPreference.setAuthorization(true);
                    } else {
                        mSharedPreference.setToken("");
                        mSharedPreference.setAuthorization(false);
                    }
                }
        );
    }

    @Override
    public Single<List<UserTimeline>> getUserTimelines(String userName, int count, String auth) {
        return mRemoteDataSource.getUserTimelines(userName, count, mSharedPreference.getToken());
    }

    @Override
    public Single<List<UserTimeline>> getOldUserTimelines(String userName, int count, String auth) {
        return mRemoteDataSource.getOldUserTimelines(userName, count, mSharedPreference.getToken());
    }

    @Override
    public Single<List<UserTimeline>> getUserTimelinesWithoutAuth(String userName, int count) {
        return downloadDataWithoutAuth(userName, count);
    }

    @Override
    public Single<List<UserTimeline>> getOldUserTimelinesWithoutAuth(String userName, int count) {
        return downloadDataWithoutAuth(userName, count);
    }

    @Override
    public boolean isUserAuthorised() {
        return mSharedPreference.isAuthorised();
    }

    private Single<List<UserTimeline>> downloadDataWithoutAuth(String userName, int count) {
        return mRemoteDataSource.twitterAuth().flatMap(auth -> {
            if (auth.getAccessToken() != null &&
                    !auth.getAccessToken().isEmpty()) {
                mSharedPreference.setAuthorization(true);
                mSharedPreference.setToken(auth.getTokenType() + " " + auth.getAccessToken());
                return mRemoteDataSource.getUserTimelines(userName, count, auth.getTokenType() + " " + auth.getAccessToken());
            } else {
                mSharedPreference.setAuthorization(false);
                mSharedPreference.setToken("");
                return Single.just(new ArrayList<>());
            }
        });
    }
}
