package com.klg.twittertestapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Url__ {

    @SerializedName("url")
    private String url;
    @SerializedName("expanded_url")
    private String expandedUrl;
    @SerializedName("display_url")
    private String displayUrl;
    @SerializedName("indices")
    private List<Integer> indices = null;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getExpandedUrl() {
        return expandedUrl;
    }

    public void setExpandedUrl(String expandedUrl) {
        this.expandedUrl = expandedUrl;
    }

    public String getDisplayUrl() {
        return displayUrl;
    }

    public void setDisplayUrl(String displayUrl) {
        this.displayUrl = displayUrl;
    }

    public List<Integer> getIndices() {
        return indices;
    }

    public void setIndices(List<Integer> indices) {
        this.indices = indices;
    }

}