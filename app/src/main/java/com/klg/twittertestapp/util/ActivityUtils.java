package com.klg.twittertestapp.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class ActivityUtils {

    public static void replaceFragmentInActivity(FragmentManager fragmentManager,
                                                 Fragment fragment, int frameId) {
        if (fragment != null && fragmentManager != null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(frameId, fragment);
            transaction.commit();
        }
    }

    public static void replaceFragmentInActivity(FragmentManager fragmentManager,
                                                 Fragment fragment, String tag) {
        if (fragment != null && fragmentManager != null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(fragment, tag);
            transaction.commit();
        }
    }
}