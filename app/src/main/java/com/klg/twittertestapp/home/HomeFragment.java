package com.klg.twittertestapp.home;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.klg.twittertestapp.R;
import com.klg.twittertestapp.data.model.UserTimeline;
import com.klg.twittertestapp.util.NetworkUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

public class HomeFragment extends Fragment {

    private SearchView mSearchView;
    private RecyclerView mRecyclerView;
    private TextView mTextViewEmptyView;
    private ProgressBar mProgressBar;

    private HomeViewModel mHomeViewModel;
    private LinearLayoutManager mLayoutManager;
    private AdapterUserTimeline mAdapter;
    private String mNewText = "";

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews(view);
        initRecyclerView(mRecyclerView);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            mHomeViewModel = HomeActivity.obtainViewModel(getActivity());
        }
        initSearchObservable(mSearchView);
        initLiveData(mHomeViewModel);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHomeViewModel.clear();
    }

    private void isThereAnyData(List<UserTimeline> userTimelines) {
        if (userTimelines.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            mTextViewEmptyView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mTextViewEmptyView.setVisibility(View.GONE);
        }
    }

    private void initViews(View view) {
        mSearchView = view.findViewById(R.id.search_view_home);
        mRecyclerView = view.findViewById(R.id.recycler_view_home);
        mTextViewEmptyView = view.findViewById(R.id.text_view_empty_view);
        mProgressBar = view.findViewById(R.id.progress_bar_home);
    }

    private void initRecyclerView(RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerAdapterUserTimeline(getContext(), new ArrayList<>());
        mRecyclerView.setAdapter((RecyclerView.Adapter) mAdapter);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
    }

    private void initLiveData(HomeViewModel mHomeViewModel) {
        mHomeViewModel.userTimelineLiveData.observe(this, userTimelines -> {
            if (userTimelines != null) {
                isThereAnyData(userTimelines);
                mAdapter.updateData(userTimelines);
            }
        });

        mHomeViewModel.progressBarLiveData.observe(this, flag -> {
            if (flag != null) {
                if (flag) {
                    mProgressBar.setVisibility(View.VISIBLE);
                } else {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = mLayoutManager.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= PAGE_SIZE) {
                if (getContext() != null && NetworkUtils.isOnline(getContext())) {
                    mHomeViewModel.downloadOldUserTimeline(mNewText, 5);
                } else {
                    showError(getResources().getString(R.string.check_internet));
                }
            }
        }
    };

    @SuppressLint("CheckResult")
    private void initSearchObservable(SearchView searchView) {
        Observable.create((ObservableOnSubscribe<String>) emitter -> searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mNewText = query;
                emitter.onNext(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mNewText = newText;
                emitter.onNext(newText);
                return false;
            }
        })).map(text -> text.toLowerCase().trim())
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinct()
                .filter(text -> !text.isEmpty())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(text -> {
                    if (getContext() != null && NetworkUtils.isOnline(getContext())) {
                        mHomeViewModel.downloadUserTimeline(text, 10);
                    } else {
                        showError(getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void showError(String message) {
        if (getContext() != null) {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

}