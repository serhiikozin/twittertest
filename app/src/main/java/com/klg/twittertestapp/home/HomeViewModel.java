package com.klg.twittertestapp.home;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.klg.twittertestapp.data.TwitterRepository;
import com.klg.twittertestapp.data.model.UserTimeline;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HomeViewModel extends AndroidViewModel {

    private TwitterRepository mTwitterRepository;
    private List<UserTimeline> mUserTimelines = new ArrayList<>();

    public MutableLiveData<List<UserTimeline>> userTimelineLiveData = new MutableLiveData<>();
    public MutableLiveData<Boolean> progressBarLiveData = new MutableLiveData<>();

    public HomeViewModel(@NonNull Application application, TwitterRepository twitterRepository) {
        super(application);
        mTwitterRepository = twitterRepository;
    }

    public void downloadUserTimeline(String userName, int count) {
        mUserTimelines.clear();
        progressBarLiveData.postValue(true);
        if (mTwitterRepository.isUserAuthorised()) {
            downloadData(userName, count);
        } else {
            downloadWithoutAuthorisation(userName, count);
        }
    }

    public void downloadOldUserTimeline(String userName, int count) {
        progressBarLiveData.postValue(true);
        if (mTwitterRepository.isUserAuthorised()) {
            downloadOldData(userName, count);
        } else {
            downloadOldWithoutAuthorisation(userName, count);
        }
    }

    public void clear() {
        mTwitterRepository.clear();
    }

    @SuppressLint("CheckResult")
    private void downloadData(String userName, int count) {
        mTwitterRepository.getUserTimelines(userName, count, "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> progressBarLiveData.postValue(false))
                .subscribe(userTimelines -> {
                            mUserTimelines.addAll(userTimelines);
                            userTimelineLiveData.postValue(mUserTimelines);
                        },
                        error -> {
                        });
    }

    @SuppressLint("CheckResult")
    private void downloadWithoutAuthorisation(String userName, int count) {
        mTwitterRepository.getUserTimelinesWithoutAuth(userName, count)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> progressBarLiveData.postValue(false))
                .subscribe(userTimelines -> {
                            mUserTimelines.addAll(userTimelines);
                            userTimelineLiveData.postValue(mUserTimelines);
                        },
                        error -> {
                        });
    }

    @SuppressLint("CheckResult")
    private void downloadOldData(String userName, int count) {
        mTwitterRepository.getOldUserTimelines(userName, count, "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> progressBarLiveData.postValue(false))
                .subscribe(userTimelines -> {
                            mUserTimelines.addAll(userTimelines);
                            userTimelineLiveData.postValue(mUserTimelines);
                        },
                        error -> {
                        });
    }

    @SuppressLint("CheckResult")
    private void downloadOldWithoutAuthorisation(String userName, int count) {
        mTwitterRepository.getOldUserTimelinesWithoutAuth(userName, count)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> progressBarLiveData.postValue(false))
                .subscribe(userTimelines -> {
                            mUserTimelines.addAll(userTimelines);
                            userTimelineLiveData.postValue(mUserTimelines);
                        },
                        error -> {
                        });
    }
}
