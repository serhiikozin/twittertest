package com.klg.twittertestapp.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.klg.twittertestapp.R;
import com.klg.twittertestapp.data.model.UserTimeline;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RecyclerAdapterUserTimeline extends RecyclerView.Adapter<RecyclerAdapterUserTimeline.UserViewHolder> implements AdapterUserTimeline {

    private final Context mContext;
    private List<UserTimeline> mUserTimelines;

    public RecyclerAdapterUserTimeline(Context context, List<UserTimeline> userTimelines) {
        mContext = context;
        mUserTimelines = userTimelines;
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder userViewHolder, int position) {
        UserTimeline timeline = mUserTimelines.get(position);
        userViewHolder.mTextViewLanguage.setText(timeline.getLang());
        userViewHolder.mTextViewTwit.setText(timeline.getText());
        String date = showDateFormate(convertStringTimeToLong(timeline.getCreatedAt()));
        userViewHolder.mTextViewTime.setText(date);
        if (timeline.getUser() != null) {
            userViewHolder.mTextViewName.setText(timeline.getUser().getName());
            Glide.with(mContext)
                    .load(timeline.getUser().getProfileImageUrl())
                    .into(userViewHolder.mImageViewUser);
        }
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_twitter_post, viewGroup, false);
        return new UserViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return mUserTimelines.size();
    }

    @Override
    public void updateData(List<UserTimeline> userTimelines) {
        mUserTimelines.clear();
        mUserTimelines.addAll(userTimelines);
        notifyDataSetChanged();
    }

    private Long convertStringTimeToLong(String timeText) {
        Long time = 0L;
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);
        if (timeText != null) {
            try {
                time = dateFormat.parse(timeText).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return time;
    }

    private String showDateFormate(Long dataFormat) {
        SimpleDateFormat dateFormatDisplay = new SimpleDateFormat("MM.dd(EEE)", Locale.ENGLISH);
        return dateFormatDisplay.format(new Date(dataFormat));
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {

        TextView mTextViewName;
        TextView mTextViewTwit;
        TextView mTextViewLanguage;
        TextView mTextViewTime;
        ImageView mImageViewUser;

        private UserViewHolder(View itemView) {
            super(itemView);
            mTextViewName = itemView.findViewById(R.id.text_view_user_name);
            mTextViewTwit = itemView.findViewById(R.id.text_view_twit);
            mTextViewLanguage = itemView.findViewById(R.id.text_view_language);
            mTextViewTime = itemView.findViewById(R.id.text_view_time);
            mImageViewUser = itemView.findViewById(R.id.image_view_user);
        }
    }
}

