package com.klg.twittertestapp.home;

import com.klg.twittertestapp.data.model.UserTimeline;

import java.util.List;

public interface AdapterUserTimeline {
    void updateData(List<UserTimeline> userTimelines);
}
